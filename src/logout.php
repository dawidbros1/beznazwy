<?php
require_once '../src/checkLoged.php';

if (isset($_SESSION['isLoged'])) {
    unset($_SESSION['isLoged']);
    header("location: ../pages/index.php");
}
