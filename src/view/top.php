<?php
require_once "../src/checkLoged.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../style/global.css">
    <link rel="stylesheet" href="../style/index.css">
    <link rel="stylesheet" href="../style/menu.css">
    <link rel="stylesheet" href="../style/footer.css">
    <link rel="stylesheet" href="../style/regLog.css">
    <link rel="stylesheet" href="../style/manageUsers.css">
    <link rel="stylesheet" href="../style/manageArticles.css">
    <link rel="stylesheet" href="../style/addArticle.css">
    <link rel="stylesheet" href="../style/editArticle.css">
    <link rel="stylesheet" href="../style/profile.css">
    <title>Bibliometr</title>
</head>

<body>
    <div id="container">
        <div id=top><?php include_once "../components/menu.php"; ?></div>