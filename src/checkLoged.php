<?php
session_start();
require_once "../entiti/user.php";
require_once "../confiig/config.php";
require_once "../entiti/Repository/userRepository.php";

if (isset($_SESSION['isLoged']) && $_SESSION['isLoged'] == true) {
    $userId = $_SESSION['userId'];
    $userRepository = new UserRepository();
    $userRepository->getUserById($userId);

    if ($userRepository->user->getStatus() == "active") {
        $role = $userRepository->user->getRole();
    } else {
        $_SESSION['isLoged'] = false;
        $_SESSION['disable'] = true;
        header("location: sorry.php");
    }
} else {
    $role = "";
}
