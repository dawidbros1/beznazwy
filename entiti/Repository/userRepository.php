<?php
class UserRepository
{

    function __construct()
    {
        $this->user = new User();
    }

    public static function getConnection()
    {
        global $dbConfig;
        $user = $dbConfig['user'];
        $pass = $dbConfig['pass'];
        $dbname = $dbConfig['name'];
        $host = $dbConfig['host'];

        try {
            $db = new \PDO('mysql:host=' . $host . ';dbname=' . $dbname . '', $user, $pass);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->query('SET NAMES utf8');
        } catch (PDOException $error) {
            echo $error->getMessage();
            exit('Database error');
        }

        return $db;
    }

    public function getUserById($id)
    {
        $db = self::getConnection();
        $sql = "SELECT * FROM users WHERE id = '$id'";
        $query = $db->prepare($sql);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);

        $this->user->setId($result['id']);
        $this->user->setFirstName($result['firstName']);
        $this->user->setLastName($result['lastName']);
        $this->user->setAffiliation($result['affiliation']);
        $this->user->setEmail($result['email']);
        $this->user->setPassword($result['password']);
        $this->user->setStatus($result['status']);
        $this->user->setRole($result['role']);
        return $this->user;
    }

    public function save()
    {
        $db = self::getConnection();

        if ($this->user->getId() !== null) {
            $query = $db->prepare('UPDATE users SET 
                firstName = :firstName, 
                lastName = :lastName,
                affiliation = :affiliation,
                email = :email,
                password = :password,
                status = :status,
                role = :role
                WHERE id = :id');
            $query->bindValue(':id', $this->user->getId(), PDO::PARAM_INT);
        } else {
            $query = $db->prepare('INSERT INTO users VALUES (NULL,:firstName,:lastName,:affiliation,:email,:password,:status,:role)');
        }

        $query->bindValue(':firstName', $this->user->getFirstName(), PDO::PARAM_STR);
        $query->bindValue(':lastName', $this->user->getLastName(), PDO::PARAM_STR);
        $query->bindValue(':affiliation', $this->user->getAffiliation(), PDO::PARAM_STR);
        $query->bindValue(':email', $this->user->getEmail(), PDO::PARAM_STR);
        $query->bindValue(':password', $this->user->getPassword(), PDO::PARAM_STR);
        $query->bindValue(':status', $this->user->getStatus(), PDO::PARAM_STR);
        $query->bindValue(':role', $this->user->getRole(), PDO::PARAM_STR);

        $query->execute();
    }

    public function getAllUsers()
    {
        $db = self::getConnection();
        $sql = "SELECT * FROM users";
        $statement = $db->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }
	public function changeStatus($id)
    {
        $this->user = $this->getUserById($id);

        if ($this->user->getStatus() == "active") {
            $this->user->setStatus("disabled");
        } else {
            $this->user->setStatus("active");
        }
        $this->save();
    }
}
