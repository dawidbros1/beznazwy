<?php
require_once "../src/view/top.php";

if (isset($_SESSION['isLoged']) && $_SESSION['isLoged'] == true) {
    if ($userRepository->user->getRole() != "admin") {
        header("location: index.php");
    }
} else {
    header("location: index.php");
}

if (isset($_POST["userIdToChangeStatus"])) {
    $userRepository->changeStatus($_POST["userIdToChangeStatus"][0]);
    header("location: manageUsers.php");
}

if (isset($_POST["userIdToDeleteAccount"])) {
    $userRepository->deleteUserById($_POST["userIdToDeleteAccount"][0]);
    header("location: manageUsers.php");
}

?>

<section id="manageUsers">
    <?php
    // //Walidacja danych
    $globalError = false;
    if (!$globalError) {
        $userRepository = new UserRepository();
        $result = $userRepository->getAllUsers();
        echo "<header>Zarządzanie Użytkownikami</header>";
        if (count($result) > 0) {
            echo '
                    <table>
                        <thead>
                            <tr>
                                <th>Nr</th>
                                <th>Imie</th>
                                <th>Nazwisko</th>
                                <th>Email</th>
                                <th>Hasło</th>
                                <th>Afiliacjja</th>
                                <th>Rola</th>
                                <th>Status</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                    ';

            foreach ($result as $key => $row) {
                if ($row['status'] == "active") $infoButtomStatus = "Zablokuj";
                else $infoButtomStatus = "Odblokuj";
                echo '
                    <tr>
                        <td>' . ($key + 1) . '</td>
                        <td>' . $row['firstName'] . '</td>
                        <td>' . $row['lastName'] . '</td>
                        <td>' . $row['email'] . '</td>
                        <td>' . $row['password'] . '</td>
                        <td>' . $row['affiliation'] . '</td>
                        <td>' . $row['role'] . '</td>
                        <td>' . $row['status'] . '</td>
                        <td>
                            <form method = "POST">
                                <input name = userIdToChangeStatus[] class = "disable" type = "text" value = ' . $row['id'] . '>
                                <input type = "submit" value = " ' . $infoButtomStatus . '">
                            </form>
                        </td>

                        <td>
                            <form method = "POST">
                                <input name = userIdToDeleteAccount[] class = "disable" type = "text" value = ' . $row['id'] . '>
                                <input type = "submit" value = "Usuń">
                            </form>
                        </td>
                    </tr>';
            }
            echo '
            </tbody>
        </table>
        ';
        }
    }
    ?>
</section>

<?php require_once "../src/view/bottom.php" ?>