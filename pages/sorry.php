<?php require_once "../src/view/top.php";

if (isset($_SESSION['disable']) && (!empty($_SESSION['disable']))) {
    echo "Twoje konto zostało zablokowane. Prosimy o kontant z administratorem";
    unset($_SESSION['disable']);
} else {
    header("location: index.php");
}

?>

<?php require_once "../src/view/bottom.php" ?>