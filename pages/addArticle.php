<?php require_once "../src/view/top.php";

if (!(isset($_SESSION['isLoged']) && $_SESSION['isLoged'] == true)) {
    header("location: index.php");
}
?>

<section id="addArticle">
    <header>Dodowanie nowego artykułu</header>

    <form method="POST" action="./../src/saveArticle.php">
        <div class="item">
            <label for=title>Tytuł: </label>
            <input type="text" name="title" id="title">
        </div>

        <div class="item" id="authors">
            <label class="labelAuthors">Autorzy: </label>
            <input type="text" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class="firstSpan">,</span>
            <input type="number" class="shapes" name="shapes[]"><span class="secondSpan">% udziału</span>
            <div id="simulateSpace"></div>
        </div>

        <div class="item">
            <div id="addAuthorButton" onclick='newAuthor()'>Dodaj nowego autora</div>
        </div>

        <div class="item">
            <label for=POP>Miejsce publikacji: </label>
            <input type="text" name="POP" id="POP">
        </div>

        <div class="item">
            <label for=DOP>Data publikacji: </label>
            <input type="text" name="DOP" id="DOP" placeholder="YYYY/MM/DD">
        </div>

        <div class="item">
            <label for=MP>Punkty ministerialne: </label>
            <input type="number" name="MP" id="MP">
        </div>

        <div class="item">
            <label for=DOI>DOI: </label>
            <input type="text" name="DOI" id="DOI">
        </div>

        <div class="item">
            <label for=IF>IF: </label>
            <input type="number" name="IF" id="IF">
        </div>

        <input type="submit" value="Dodaj">

    </form>

</section>

<script>
    function newAuthor() {
        var authors = document.getElementById("authors");
        var arrayValue = [];
        var arrayShapes = [];
        var myAuthors = document.getElementsByClassName('authors');
        var myShapes = document.getElementsByClassName('shapes');
        var maxAuthors = myAuthors.length;

        for (var i = 0; i < maxAuthors; i++) {
            arrayValue[i] = myAuthors[i].value;
            arrayShapes[i] = myShapes[i].value;
        }

        authors.innerHTML = '<label class="labelAuthors">Autorzy: </label><input type="text" value = "' + arrayValue[0] + '" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shapes" value = "' + arrayShapes[0] + '"  name="shapes[]"><span class = "secondSpan">% udziału </span><div id="simulateSpace"></div>';

        for (var i = 0; i < maxAuthors - 1; i++) {
            authors.innerHTML += '<label class="labelAuthors">Autorzy: </label><input type="text" value = "' + arrayValue[i + 1] + '" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shapes" value = "' + arrayShapes[i + 1] + '" name="shapes[]"><span class = "secondSpan">% udziału </span><div class="deleteAuthor" onclick = deleteAuthor(' + (i + 1) + ') >Usuń</div>';
        }

        authors.innerHTML += '<label class="labelAuthors">Autorzy: </label><input type="text" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shapes" name="shapes[]"><span class = "secondSpan">% udziału </span><div class="deleteAuthor" onclick = deleteAuthor(' + maxAuthors + ') >Usuń</div>';

    }

    function deleteAuthor(index) {
        var authors = document.getElementById("authors");
        var myAuthors = document.getElementsByClassName('authors');
        var maxAuthors = myAuthors.length;
        var myShapes = document.getElementsByClassName('shapes');
        var myLabel = document.getElementsByClassName('labelAuthors');
        var myAuthor = document.getElementsByClassName('authors');
        var myButton = document.getElementsByClassName('deleteAuthor');
        var myShapes = document.getElementsByClassName('shapes');
        var mySpan1 = document.getElementsByClassName('firstSpan');
        var mySpan2 = document.getElementsByClassName('secondSpan');

        var arrayValue = [];
        var arrayShapes = [];

        for (var i = 0; i < maxAuthors; i++) {
            arrayValue[i] = myAuthors[i].value;
            arrayShapes[i] = myShapes[i].value;
        }

        myAuthor[index].remove();
        myLabel[index].remove();
        myShapes[index].remove();
        mySpan1[index].remove();
        mySpan2[index].remove();
        myButton[index - 1].remove();

        authors.innerHTML = '<label class="labelAuthors">Autorzy: </label><input type="text" value = "' + arrayValue[0] + '" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shapes" value = "' + arrayShapes[0] + '"  name="shapes[]"><span class = "secondSpan">% udziału </span><div id="simulateSpace"></div>';

        var j = 0;

        for (var i = 0; i < maxAuthors - 2; i++) {
            if (i == index - 1) j = 1;
            authors.innerHTML += '<label class="labelAuthors">Autorzy: </label><input type="text" value = "' + arrayValue[i + 1 + j] + '" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shapes" value = "' + arrayShapes[i + 1 + j] + '" name="shapes[]"><span class = "secondSpan">% udziału </span><div class="deleteAuthor" onclick = deleteAuthor(' + (i + 1) + ') >Usuń</div>';
        }
    }
</script>

<?php require_once "../src/view/bottom.php" ?>