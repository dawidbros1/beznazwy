<?php require_once "../src/view/top.php";

if (!(isset($_SESSION['isLoged']) && $_SESSION['isLoged'] == true)) {
    header("location: index.php");
}

?>

<?php

//Pobranie liczby artykułów danego użytkownika

echo '
<section id="profile">

    <div id="upPanel">
        <div id="name"></div>
        <div id="affiliation" class = "medium-low"> <span class = "bold medium">Afiliacja:</span></div>
    </div>
    <div id="bottomPanel">
        <span class = "bold big">Lista artykułów: </span>
        <div id = "articleList">
            <ol>
';

echo '
            </ol>
        </div>
    </div>
</section>
';


?>
<?php require_once "../src/view/bottom.php" ?>